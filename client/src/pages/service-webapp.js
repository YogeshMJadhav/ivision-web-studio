import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Fade from 'react-reveal/Fade';

const WebApp = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <Fade>
    <div className="wrapper">
    <div className="pagebanner pagebannerLess">
            <div className="pagebannerMax">
                <h1 className="white">Web Application </h1>
                 <h3 className="white mb-3">Creating online environments</h3> 
                {/* <!-- <p className="white">The easiest way to get started is to use Ghost(Pro). If you prefer to self-host, we strongly recommend an Ubuntu server with at least 1GB of memory to run Ghost.</p> --> */}
                <div>
                    <img src="images/tools/react-icon.png" alt="ReactJS" title="ReactJS" width="40" className="mr-1" />
                    <img src="images/tools/angular-icon.png" alt="AngularJS" title="AngularJS" width="40" className="mr-1" />
                    <img src="images/tools/js.png" alt="NodeJS" title="NodeJS" width="40" className="mr-1" />
                    <img src="images/tools/laravel-icon.png" alt="Laravel" title="Laravel" width="40" className="mr-1" />
                </div>
                
            </div>
        </div>

        
<div className="whatWeSection pb-5">
    <div className="container container-less">
        <div className="row">
            <div className="col-12">
                <div className="bg-white shadow-1 brb4 p-5 ">

<div className="row mb-3">
    <div className="col-12">
        <strong>Tools:- </strong> 
         ReactJS + Redux/ AngularJS /  NodeJS / Laravel
    </div>    
</div>

<div className="row">
    <div className="col-12 font-large" >
        <p>
            Ivision Web Studio, recognized as the best web app development company promises to offer you top notch web application development services at the most competitive pricing. We have specialized in providing flexible and high quality web apps. Our skilled team possess significant expertise to implement modern technologies for web solutions. WE have completed numerous projects successfully and have delivered secure, reliable and web enabled solutions.
        </p>        
    </div>    
</div>


<div className="mt-4"> 
<div className="row">
    <div className="col-12">
        <h3> Web Application Development</h3>          
    </div>    
</div>


<div className="row">
    <div className="col-12">
        <p>
            Web Application Development is the creation of application programs that reside on remote servers and are delivered to the user's device over the Internet.
for starters, web applications don’t require any installation and that means that they can be used almost instantly. At the same time, this also means that nearly all web applications are cloud-based by definition

        </p>
    </div>
</div>    





</div>




                </div>
            </div>
        </div>
    </div>

</div>        
    </div>
    </Fade>
  </Layout>
)
export default WebApp;
