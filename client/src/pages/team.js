import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide';

const Team = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
     <Fade >
     <div className="wrapper">
     <div className="pagebanner">
            <div className="pagebannerMax">
                <h1 className="white">Team</h1>
                 <h3 className="mb-4 white">The people who work at iVision Web Studio share the vision and values of our community.</h3> 
                {/* <!-- <p className="white">The easiest way to get started is to use Ghost(Pro). If you prefer to self-host, we strongly recommend an Ubuntu server with at least 1GB of memory to run Ghost.</p> --> */}
                
            </div>
        </div>

        


<div className="whatWeSection pt-5">
            <div className="container">
                
                <div className="row">

                    <Slide bottom>  
                    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/rahul_rawade.jpg" title="Rahul Rawade" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Rahul Rawade</h5>
                                <p className="card-text text-blue "> CEO </p>                               
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/girish boke.jpg" title="Girish Boke" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Girish Boke</h5>
                                <p className="card-text text-blue "> Team Manager </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/preity_uttekar.jpg" title="Preity Uttekar" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Preity Uttekar</h5>
                                <p className="card-text text-blue "> Project Coordinator </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/jay_jadhav.jpg" title="Jaywardhan Jadhav" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Jaywardhan Jadhav</h5>
                                <p className="card-text text-blue "> UI/UX Software Engineer </p>
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/ronak_rawade.jpg" title="Ronak Rawade" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Ronak Rawade</h5>
                                <p className="card-text text-blue "> UI/UX Software Engineer </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/yogesh_jadhav.jpg" title="Yogesh Jadhav" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Yogesh Jadhav</h5>
                                <p className="card-text text-blue "> Software Developer </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/trushant_bhangare.jpg" title="Trushant Bhangare" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Trushant Bhangare</h5>
                                <p className="card-text text-blue "> Software Developer </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/aniket_shinde.jpg" title="Aniket Shinde" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Aniket Shinde</h5>
                                <p className="card-text text-blue "> Team Leader </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>





                     <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/akshay_chidre.jpg" title="Akshay Chidre" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Akshay Chidre</h5>
                                <p className="card-text text-blue "> UI/UX Developer</p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/baldev_pardeshi.jpg" title="Baldev Pardeshi" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Baldev Pardeshi</h5>
                                <p className="card-text text-blue "> UI/UX Developer </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/abhijeet_jachak.jpg" title="Abhijeet Jachak" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Abhijeet Jachak</h5>
                                <p className="card-text text-blue "> UI Developer </p>
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/mukund_kadu.jpg" title="Mukund Kadu" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Mukund Kadu</h5>
                                <p className="card-text text-blue "> UI/UX Designer </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    <Slide bottom>  <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-5">
                        <div className="card border-0 text-center py-3">   
                            <div className="card-body">
                                <div className="mb-3">
                                    <img src="../images/team/pravin_jadhav.jpg" title="Pravin Jadhav" alt="Android-icon" width="120" className="rounded-circle"/>
                                </div>
                                <h5 className="">Pravin Jadhav</h5>
                                <p className="card-text text-blue "> UI/UX Designer </p>
                                
                            </div>
                        </div>
                    </div>
                    </Slide>

                    

                </div>
            </div>

          
        </div>   
    </div>
    </Fade>
  </Layout>
)
export default Team;
