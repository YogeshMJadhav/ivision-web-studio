import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Fade from 'react-reveal/Fade';

const WebSite = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <Fade>
    <div className="wrapper">
    <div className="pagebanner pagebannerLess">
            <div className="pagebannerMax">
                <h1 className="white">Website Development</h1>
                 <h3 className="white mb-3">Building smarter websites</h3> 
                {/* <!-- <p className="white">The easiest way to get started is to use Ghost(Pro). If you prefer to self-host, we strongly recommend an Ubuntu server with at least 1GB of memory to run Ghost.</p> --> */}
                <div>
                    <img src="images/tools/cms.png" alt="CMS" title="CMS" width="40" className="mr-1" />
                    <img src="images/tools/ecommerce2.png"  alt="e-commerce" title="e-commerce" width="40" className="mr-1" />
                    <img src="images/tools/responsive2.png"  alt="Responsive" title="Responsive" width="40" className="mr-1" />
                </div>
                
            </div>
        </div>

        
<div className="whatWeSection pb-5">
    <div className="container">
        <div className="row">
            <div className="col-12">
                <div className="bg-white shadow-1 brb4 p-5 ">

<div className="row mb-3">
    <div className="col-12">
        <strong>Tools:- </strong> 
        Opencart, WooCommerce, Magento. /   Wordpress. /  Bootstrap, Foundation. /  HTML 5, CSS 3, Javascript. 
    </div>    
</div>

<div className="row">
    <div className="col-12 font-large" >
        iVision Studio specializes in offering top-notch services in the area of web development. We work with important elements when developing a website for our clients. We make sure that the theme of the website aligns with the theme of their business/niche.
        In addition, we excel in the development of secure and user intuitive content management systems.
    </div>    
</div>


<div className="mt-5"> 
<div className="row">
    <div className="col-12">
        <h3> What is the Website Development ? </h3>          
    </div>    
</div>
<div className="row">
    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <img src="images/tools/wordpress2.png" alt="Opencart" title="Opencart" width="40" className="mr-1" />
            <h5> CMS ( content management system )  </h5>          
            <p >
                A content management system is a software application or set of related programs that are used to create and manage digital content.
            </p>
        </div>    
    </div>

    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <img src="images/tools/wooocmmerce2.png" alt="WooCommerce" title="WooCommerce" width="40" className="mr-1" />
            <img src="images/tools/magento2.png" alt="Magento" title="Magento" width="40" className="mr-1" />
            <img src="images/tools/opencart2.png" alt="Opencart" title="Opencart" width="40" className="mr-1" />
            <h5> Ecommerce  </h5>          
            <p>
                Ecommerce, also known as electronic commerce or internet commerce, refers to the buying and selling of goods or services using the internet
            </p>
        </div>    
    </div> 

    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <img src="images/tools/bootstrap2.png" alt="WooCommerce" title="WooCommerce" width="40" className="mr-1" />
            <img src="images/tools/foundation2.png" alt="WooCommerce" title="WooCommerce" width="40" className="mr-1" />
            <h5> Responsive Web Design  </h5>          
            <p>
                We develop responsive layouts, Bootstrap-based / Foundation Projects. We test website on all major devices and browsers. As responsive website is the first approach that shows website development and design should respond to all the user’s request regardless of device they use
            </p>
        </div>    
    </div>  
    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <img src="images/tools/monkey.png" alt="WooCommerce" title="WooCommerce" width="40" className="mr-1" /> 
            <h5> Email Template Development   </h5>          
            <p>
                Email templates allow you to customize the formatting and text of emails sent by users who share your content. Templates can be text-only, or HTML and text, in which case the user’s email client will determine which is displayed. 
            </p>
        </div>    
    </div>    
</div>

</div>


                </div>
            </div>
        </div>
    </div>

</div>        
   </div>
    </Fade>
  </Layout>
)
export default WebSite;
