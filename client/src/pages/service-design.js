import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Fade from 'react-reveal/Fade';

const Design = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <Fade>
    <div className="wrapper">
    <div className="pagebanner pagebannerLess">
            <div className="pagebannerMax">
                <h1 className="white"> UI / UX Design </h1>
                 <h3 className="white mb-3">UI/UX Both Work Closely Together</h3> 
                {/* <!-- <p className="white">The easiest way to get started is to use Ghost(Pro). If you prefer to self-host, we strongly recommend an Ubuntu server with at least 1GB of memory to run Ghost.</p> --> */}
                <div>
                    <img src="images/tools/photoshop.png" alt="Photoshop" title="Photoshop" width="40" className="mr-1" />
                    <img src="images/tools/Illustrator.png" alt="Illustrator" title="Illustrator" width="40" className="mr-1" />
                    <img src="images/tools/wire_sw.png" alt="WireframesSoftware" title="WireframesSoftware" width="40" className="mr-1" />
                    
                </div>
                
            </div>
        </div>

        
<div className="whatWeSection pb-5">
    <div className="container container-less">
        <div className="row">
            <div className="col-12">
                <div className="bg-white shadow-1 brb4 p-5 ">

<div className="row mb-3">
    <div className="col-12">
        <strong>Tools:- </strong> 
        Photoshop / Illustrator / Wireframes software 
    </div>    
</div>

<div className="row">
    <div className="col-12 font-large" >
        <p>
                With our Design Thinking technology, rich experience, and a deep understanding of the industry we have achieved new heights across various industries.
        </p>
        <p>
            iVision Studio considers designing modern and updated websites for its clients as a backbone of its business. We are familiar with vast techniques and tools to design attractive and professional websites.
            We work with a team of excellent and proficient website designers, who are experienced in using UX/UI design to design modern websites for you.
         </p>        
    </div>    
</div>


{/* <!-- <div className="mt-4"> 
<div className="row">
    <div className="col-12">
        <h3> Mobile Application Development</h3>          
    </div>    
</div>


<div className="row">
    <div className="col-12">
        <p>
            A majority of the users prefer using
            their smartphones to navigate a
            website or an app. The mobile
            applications should be developed
            in congruence with a user-friendly
            interface, and we strive to develop
            top-notch and accessible mobile
            apps for our clients.
        </p>
    </div>
</div>    

</div> --> */}




                </div>
            </div>
        </div>
    </div>

</div>        
   </div>
    </Fade>
  </Layout>
)
export default Design;
