import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Fade from 'react-reveal/Fade';

const Jam = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <Fade>
    <div className="wrapper">
    <div className="pagebanner pagebannerLess">
            <div className="pagebannerMax">
                <h1 className="white">JAM Development </h1>
                 <h3 className="white mb-3">Modern Web Development Architecture</h3> 
                {/* <!-- <p className="white">The easiest way to get started is to use Ghost(Pro). If you prefer to self-host, we strongly recommend an Ubuntu server with at least 1GB of memory to run Ghost.</p> --> */}
                <div>
                    <img src="images/tools/gatsby-icon.png" alt="Gatsby" title="Gatsby" width="40" className="mr-1" />
                    <img src="images/tools/Jekyll-icon.png"  alt="Jekyll" title="Jekyll" width="40" className="mr-1" />
                    <img src="images/tools/contentfull-icon.png"  alt="Contentfull" title="Contentfull" width="40" className="mr-1" />
                </div>
                
            </div>
        </div>

        
<div className="whatWeSection pb-5">
    <div className="container">
        <div className="row">
            <div className="col-12">
                <div className="bg-white shadow-1 brb4 p-5 ">

<div className="row mb-3">
    <div className="col-12">
        <strong>Tools:- </strong> 
         Gatsby /Hugo/Jekyll / Contentful / Netlify 
    </div>    
</div>

<div className="row">
    <div className="col-12 font-large" >
        The JAM Development is not about specific technologies. It’s a new way of building websites and apps that delivers better performance, higher security, lower cost of scaling, and a better developer experience.        
    </div>    
</div>


<div className="mt-4"> 
<div className="row">
    <div className="col-12">
        <h3> What is the JAM Development ? </h3>          
    </div>    
</div>

<div className="row">
    <div className="col-md-4 mt-3">
        <div className="h-100 p-3 layout-border br10 layout-border br10">
            <h5> Javascript </h5>          
            <p >
    Any dynamic programming during the request/response cycle is handled by JavaScript, running entirely on the client. This could be any frontend framework, library, or even vanilla JavaScript.
            </p>
        </div>    
    </div>

    <div className="col-md-4 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <h5> APIs </h5>          
            <p>
    All server-side processes or database actions are abstracted into reusable APIs, accessed over HTTPS with JavaScript. These can be custom-built or leverage third-party services.
            </p>
        </div>    
    </div> 

    <div className="col-md-4 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <h5> Markup  </h5>          
            <p>
    Templated markup should be prebuilt at deploy time, usually using a site generator for content sites, or a build tool for web apps.
            </p>
        </div>    
    </div>     
</div>
















</div>

<div className="mt-5"> 
<div className="row">
    <div className="col-12">
        <h3> Why the JAM Development? </h3>          
    </div>    
</div>
<div className="row">
    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <h5> Better Performance </h5>          
            <p >
    Why wait for pages to build on the fly when you can generate them at deploy time? When it comes to minimizing the time to first byte, nothing beats pre-built files served over a CDN.

            </p>
        </div>    
    </div>

    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <h5> Higher Security </h5>          
            <p>
    With server-side processes abstracted into microservice APIs, surface areas for attacks are reduced. You can also leverage the domain expertise of specialist third-party services.
            </p>
        </div>    
    </div> 

    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <h5> Better Developer Experience  </h5>          
            <p>
    Loose coupling and separation of controls allow for more targeted development and debugging, and the expanding selection of CMS options for site generators remove the need to maintain a separate stack for content and marketing.
            </p>
        </div>    
    </div>  
    <div className="col-md-6 mt-3">
        <div className="h-100 p-3 layout-border br10" >
            <h5> Cheaper, Easier Scaling  </h5>          
            <p>
    When your deployment amounts to a stack of files that can be served anywhere, scaling is a matter of serving those files in more places. CDNs are perfect for this, and often include scaling in all of their plans.
            </p>
        </div>    
    </div>    
</div>

</div>


                </div>
            </div>
        </div>
    </div>

</div>        
    </div>
    </Fade>
  </Layout>
)
export default Jam;
