import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Fade from 'react-reveal/Fade';

const Maintenance = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <Fade>
    <div className="wrapper">
     <div className="pagebanner pagebannerLess">
            <div className="pagebannerMax">
                <h1 className="white"> Redesign & Maintenance </h1>
                 <h3 className="white mb-3">Our website maintenance services are tailor made to meet your needs</h3> 
                {/* <!-- <p className="white">The easiest way to get started is to use Ghost(Pro). If you prefer to self-host, we strongly recommend an Ubuntu server with at least 1GB of memory to run Ghost.</p> -->
                <!-- <div>
                    <img src="images/tools/ios-icon.png" alt="IOS" title="IOS" width="40" className="mr-1" />
                    <img src="images/tools/android-con.png" alt="Android" title="Android" width="40" className="mr-1" />
                    <img src="images/tools/react-icon.png" alt="React" title="React" width="40" className="mr-1" />
                    <img src="images/tools/ionic-icon.png" alt="Ionic" title="Ionic" width="40" className="mr-1" />
                </div> --> */}
                
            </div>
        </div>

        
<div className="whatWeSection pb-5">
    <div className="container container-less">
        <div className="row">
            <div className="col-12">
                <div className="bg-white shadow-1 brb4 p-5 ">

<div className="row mb-3">
    {/* <!-- <div className="col-12">
        <strong>Tools:- </strong> 
        Photoshop / Illustrator / Wireframes software 
    </div>     --> */}
</div>

<div className="row">
    <div className="col-12 font-large" >
        <p>
            iVision Studio offers redesign maintenance services to its customers, and we take all the measurements to help our clients with updating their websites. We make sure that your website is updated as well as up-to-date, and we scan it thoroughly to avoid any glitches from it.
        </p>        
    </div>    
</div>


{/* <!-- <div className="mt-4"> 
<div className="row">
    <div className="col-12">
        <h3> Mobile Application Development</h3>          
    </div>    
</div>


<div className="row">
    <div className="col-12">
        <p>
            A majority of the users prefer using
            their smartphones to navigate a
            website or an app. The mobile
            applications should be developed
            in congruence with a user-friendly
            interface, and we strive to develop
            top-notch and accessible mobile
            apps for our clients.
        </p>
    </div>
</div>    

</div> --> */}




                </div>
            </div>
        </div>
    </div>

</div>        
   </div>
    </Fade>
  </Layout>
)
export default Maintenance;
